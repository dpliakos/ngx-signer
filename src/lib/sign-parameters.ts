// tslint:disable:quotemark
// tslint:disable:max-line-length
export const SIGN_PARAMETERS = {
    "model": "ReportTemplates",
    "settings": {
        "i18n": {
            "el": {
                "Enable this feature to sign documents during export": "Ενεργοποιήστε αυτή την επιλογή για την ψηφιακή υπογραφή των εγγράφων",
                "Set the url of the server report template": "Ορίστε την ηλεκτρονική διαδρομή του προτύπου αναφοράς του διακομιστή",
                "Enable this feature to generate a document number based on server-side configuration about document numbering": "Ενεργοποιήστε αυτή την επιλογή για την δημιουργία ενός αριθμού εγγράφου σύμφωνα με τις ρυθμίσεις του διακομιστή για την παραγωγή αριθμών εγγράφων",
                "Document number series": "Σειρά αριθμών εγγράφων",
                "SignerServiceFailed": "Η υπηρεσία ψηφιακής υπογραφής φαίνεται ότι είναι ανενεργή ή η κατάστασή της δεν μπορεί να προσδιοριστεί. Θα πρέπει να ενεργοποιήσετε την υπηρεσία για να υπογράψετε ψηφιακά ένα έγγραφό.",
                "Type certificate keystore password": "Πληκτρολογήστε τον κωδικό πρόσβασης του αποθετηρίου πιστοποιητικών",
                "Type certificate keystore username": "Πληκτρολογήστε το όνομα χρήστη του αποθετηρίου πιστοποιητικών",
                "Keep me signed-in during this session": "Να μείνω συνδεδεμένος κατά τη διάρκεια αυτής της συνεδρίας",
                "Get certificate list": "Παραλαβή λίστας πιστοποιητικών",
                "Select certificate": "Επιλέξτε ένα ψηφιακό πιστοποιητικό",
                "Enable this option to automatically publish document after signing. Make sure you check the digital signature of the document before publishing it.": "Ενεργοποιήστε αυτή την επιλογή για την αυτόματη δημοσίευση του εγγράφου μετά την ψηφιακή υπογραφή. Σιγουρευτείτε ότι έχετε ελέγξει την ψηφιακή υπογραφή του εγγράφου προτού το δημοσιεύσετε."
            },
            "en": {
                "SignerServiceFailed": "Signer service seems that is not running or its status cannot be determined. You should try to start signer service to enable digital signing."
            }
        }
    },
    "params": {
        "signReport": true
    },
    "components": [
        {
            "type": "fieldset",
            "components": [
                {
                    "columns": [
                        {
                            "components": [
                                {
                                    "label": "Enable digital signing",
                                    "defaultValue": true,
                                    "validate": {
                                        "unique": false,
                                        "multiple": false
                                    },
                                    "key": "signReport",
                                    "type": "checkbox",
                                    "input": true,
                                    "description": "Enable this feature to sign documents during export",
                                    "disabled": true
                                }
                            ],
                            "width": 12
                        },
                        {
                            "components": [
                                {
                                    "label": "Publish document",
                                    "defaultValue": false,
                                    "validate": {
                                        "unique": false,
                                        "multiple": false
                                    },
                                    "key": "published",
                                    "type": "checkbox",
                                    "customConditional": "show = form.params.documentCanBePublished",
                                    "input": true,
                                    "description": "Enable this option to automatically publish document after signing. Make sure you check the digital signature of the document before publishing it.",
                                    "disabled": true
                                }
                            ],
                            "width": 12
                        },
                        {
                            "components": [
                                {
                                    "label": "HTML",
                                    "tag": "div",
                                    "attrs": [
                                    ],
                                    "content": "<div class=\"alert alert-warning\" role=\"alert\">{{ instance.i18next.translator.translate('SignerServiceFailed')}}</div>",
                                    "refreshOnChange": false,
                                    "tableView": false,
                                    "key": "signerServiceStatus",
                                    "conditional": {
                                        "show": true,
                                        "when": "signerServiceStatus",
                                        "eq": 0
                                    },
                                    "type": "htmlelement",
                                    "input": false
                                }
                            ],
                            "width": 12
                        },
                        {
                            "components": [
                              {
                                "label": "Type certificate keystore username",
                                "tableView": false,
                                "disabled": true,
                                "key": "typeKeyStoreUsername",
                                "spellcheck": false,
                                "type": "textfield",
                                "input": true,
                                "protected": false,
                                "customConditional": "show = form.params.signReport && form.params.requiresUsernamePassword",
                                "logic": [
                                    {
                                        "name": "disableUsernameLogic",
                                        "trigger": {
                                            "type": "simple",
                                            "simple": {
                                                "show": true,
                                                "when": "signerAlreadyAuthenticated",
                                                "eq": true
                                            }
                                        },
                                        "actions": [
                                            {
                                                "name": "disableUsernameAction",
                                                "type": "property",
                                                "property": {
                                                    "label": "Disabled",
                                                    "value": "disabled",
                                                    "type": "boolean"
                                                },
                                                "state": true
                                            }
                                        ]
                                    },
                                    {
                                        "name": "enableUsernameLogic",
                                        "trigger": {
                                            "type": "simple",
                                            "simple": {
                                                "show": true,
                                                "when": "signerAlreadyAuthenticated",
                                                "eq": false
                                            }
                                        },
                                        "actions": [
                                            {
                                                "name": "enableUsernameAction",
                                                "type": "property",
                                                "property": {
                                                    "label": "Disabled",
                                                    "value": "disabled",
                                                    "type": "boolean"
                                                },
                                                "state": false
                                            }
                                        ]
                                    }
                                ]
                            },
                                {
                                    "label": "Type certificate keystore password",
                                    "tableView": false,
                                    "disabled": true,
                                    "key": "typeKeyStorePassword",
                                    "spellcheck": false,
                                    "type": "password",
                                    "input": true,
                                    "protected": false,
                                    "conditional": {
                                        "show": true,
                                        "when": "signReport",
                                        "eq": "true"
                                    },
                                    "logic": [
                                        {
                                            "name": "disablePasswordLogic",
                                            "trigger": {
                                                "type": "simple",
                                                "simple": {
                                                    "show": true,
                                                    "when": "signerAlreadyAuthenticated",
                                                    "eq": true
                                                }
                                            },
                                            "actions": [
                                                {
                                                    "name": "disablePasswordAction",
                                                    "type": "property",
                                                    "property": {
                                                        "label": "Disabled",
                                                        "value": "disabled",
                                                        "type": "boolean"
                                                    },
                                                    "state": true
                                                }
                                            ]
                                        },
                                        {
                                            "name": "enablePasswordLogic",
                                            "trigger": {
                                                "type": "simple",
                                                "simple": {
                                                    "show": true,
                                                    "when": "signerAlreadyAuthenticated",
                                                    "eq": false
                                                }
                                            },
                                            "actions": [
                                                {
                                                    "name": "enablePasswordAction",
                                                    "type": "property",
                                                    "property": {
                                                        "label": "Disabled",
                                                        "value": "disabled",
                                                        "type": "boolean"
                                                    },
                                                    "state": false
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "label": "Keep me signed-in during this session",
                                    "tableView": false,
                                    "disabled": true,
                                    "key": "keepMeSignedInDuringThisSession",
                                    "type": "checkbox",
                                    "input": true,
                                    "defaultValue": true,
                                    "conditional": {
                                        "show": true,
                                        "when": "signReport",
                                        "eq": "true"
                                    },
                                    "logic": [
                                        {
                                            "name": "disableRememberMeLogic",
                                            "trigger": {
                                                "type": "simple",
                                                "simple": {
                                                    "show": true,
                                                    "when": "signerAlreadyAuthenticated",
                                                    "eq": true
                                                }
                                            },
                                            "actions": [
                                                {
                                                    "name": "disableRememberMeAction",
                                                    "type": "property",
                                                    "property": {
                                                        "label": "Disabled",
                                                        "value": "disabled",
                                                        "type": "boolean"
                                                    },
                                                    "state": true
                                                }
                                            ]
                                        },
                                        {
                                            "name": "enableRememberMeLogic",
                                            "trigger": {
                                                "type": "simple",
                                                "simple": {
                                                    "show": true,
                                                    "when": "signerAlreadyAuthenticated",
                                                    "eq": false
                                                }
                                            },
                                            "actions": [
                                                {
                                                    "name": "enableRememberMeAction",
                                                    "type": "property",
                                                    "property": {
                                                        "label": "Disabled",
                                                        "value": "disabled",
                                                        "type": "boolean"
                                                    },
                                                    "state": false
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "label": "Get certificate list",
                                    "action": "event",
                                    "showValidations": false,
                                    "theme": "warning",
                                    "size": "sm",
                                    "tableView": false,
                                    "disabled": true,
                                    "key": "submit1",
                                    "type": "button",
                                    "input": true,
                                    "event": "certificates",
                                    "conditional": {
                                        "show": true,
                                        "when": "signReport",
                                        "eq": "true"
                                    },
                                    "logic": [
                                        {
                                            "name": "disableGetCertsLogic",
                                            "trigger": {
                                                "type": "simple",
                                                "simple": {
                                                    "show": true,
                                                    "when": "signerAlreadyAuthenticated",
                                                    "eq": true
                                                }
                                            },
                                            "actions": [
                                                {
                                                    "name": "disableGetCertsAction",
                                                    "type": "property",
                                                    "property": {
                                                        "label": "Disabled",
                                                        "value": "disabled",
                                                        "type": "boolean"
                                                    },
                                                    "state": true
                                                }
                                            ]
                                        },
                                        {
                                            "name": "enableGetCertsLogic",
                                            "trigger": {
                                                "type": "simple",
                                                "simple": {
                                                    "show": true,
                                                    "when": "signerAlreadyAuthenticated",
                                                    "eq": false
                                                }
                                            },
                                            "actions": [
                                                {
                                                    "name": "enableGetCertsAction",
                                                    "type": "property",
                                                    "property": {
                                                        "label": "Disabled",
                                                        "value": "disabled",
                                                        "type": "boolean"
                                                    },
                                                    "state": false
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "label": "Select certificate",
                                    "tableView": true,
                                    "data": {
                                        "values": [
                                            {
                                                "label": "---",
                                                "value": ""
                                            }
                                        ]
                                    },
                                    "conditional": {
                                        "show": true,
                                        "when": "signReport",
                                        "eq": "true"
                                    },
                                    "selectThreshold": 0.3,
                                    "key": "signCertificate",
                                    "type": "select",
                                    "validate": {
                                        "required": true
                                    },
                                    "lazyLoad": false,
                                    "input": true,
                                    "indexeddb": {
                                        "filter": {}
                                    }
                                }
                            ],
                            "width": 12
                        }
                    ],
                    "customClass": "text-dark",
                    "hideLabel": true,
                    "type": "columns",
                    "input": false
                }
            ]
        },
        {
            "type": "button",
            "label": "Submit",
            "key": "submit",
            "disableOnInvalid": true,
            "input": true,
            "hidden": true
        }
    ]
};
