import { TestBed } from '@angular/core/testing';

import { NgxSignerService } from './ngx-signer.service';

describe('NgxSignerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgxSignerService = TestBed.get(NgxSignerService);
    expect(service).toBeTruthy();
  });
});
